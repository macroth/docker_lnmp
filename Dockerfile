FROM debian:jessie

MAINTAINER macroth@gmail.com

ARG mysql_port=3306
ARG host_port=80

USER root

# Install lnmp
RUN apt-get update && apt-get install -y curl ca-certificates --no-install-recommends && \
    curl -sL http://mirrors.linuxeye.com/lnmp-full.tar.gz | tar xz --strip-components=1 && \
    (echo 4 \
    sleep 1 \
    echo \
    sleep 1 \
    echo 7 \
    sleep 1 \
    echo \
    echo )| ./install.sh

VOLUME /usr/local/nginx/
VOLUME /usr/local/mysql/
VOLUME /home/wwwroot/
VOLUME /home/wwwlogs/

EXPOSE ${mysql_port}
EXPOSE ${host_port}

WORKDIR /home/wwwroot